import doctest
import collateral as ll
doctest.testmod(ll.tools)
doctest.testmod(ll.decorators)
doctest.testmod(ll.exception)
doctest.testmod(ll.parameters)
doctest.testmod(ll.collateral)
doctest.testmod(ll.functions)
